bin/sincronizacion: obj/util.o obj/main.o
	gcc   $^ -o bin/sincronizacion -fsanitize=address,undefined

obj/main.o: src/main.c 
	gcc -Wall -c $^ -I include/ -o obj/main.o -g -pthread

obj/util.o: src/util.c
	gcc -Wall -c $^ -I include/ -o obj/util.o -g -pthread


.PHONY: clean
clean:
	rm bin/* obj/*