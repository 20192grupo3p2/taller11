#include <stdio.h>
#include <pthread.h>
#include <getopt.h>
#include <stdlib.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include "../include/util.h"


int cflag=0;
int pflag=0;
int nflag=0;
int eflag=0;
int sflag=0;

int main (int argc, char **argv){
	int opt=0;
	int nhiloscon=0;
	int nhilosprod=0;
	int tambuff=0;
	int nelementsprod=0;

	while((opt=getopt(argc,argv,"c:p:n:e:s"))!=-1){
		switch(opt){

			case 'c':
				
				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					cflag=1;
					nhiloscon=atoi(optarg);
					break;
				}
				
			
			case 'p':
					
					if(atol(optarg)<0){
						printf("No se permiten numeros negativos..\n");
						return -1;
					}
					else{
						pflag=1;
						nhilosprod=atoi(optarg);
						break;
					}
					
			case 'n':
				
				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					nflag=1;
					tambuff=atoi(optarg);
					break;
				}

			case 'e':

				if(atol(optarg)<0){
					printf("No se permiten numeros negativos..\n");
					return -1;
				}
				else{
					eflag=1;
					nelementsprod=atoi(optarg);		
					break;
				}

			case 's':

				sflag=1;
				break;

			case '?':
				default:
					printf("Argumento no valido.\n");
					printf("Uso correcto:\n");
					printf("%s -c # -p # -n # -e # -s\n",argv[0]);
					printf("-n # -> número de hilos consumidores.\n");
					printf("-p # -> número de hilos productores.\n");
					printf("-n # -> tamaño del buffer.\n");
					printf("-e # -> número de elementos a producir.\n");
					printf("-s   -> si existe, hará que el programa use semaforos.\n");
					return -1;
		}

	}

	if(argc>optind){
		printf("Argumento no valido.\n");
		printf("Uso correcto:\n");
		printf("%s -c # -p # -n # -e # -s\n",argv[0]);
		printf("-n # -> número de hilos consumidores.\n");
		printf("-p # -> número de hilos productores.\n");
		printf("-n # -> tamaño del buffer.\n");
		printf("-e # -> número de elementos a producir.\n");
		printf("-s   -> si existe, hará que el programa use semaforos.\n");
		return -1;
	}

	if(cflag & pflag & nflag & eflag & sflag){
		// USANDO SEMAFOROSSS
		sbuf_t *sb = (sbuf_t*)malloc(sizeof(sbuf_t));
		sbuf_init(sb, tambuff);
		int ArrProductores[nhilosprod];
		int ArrConsumidor[nhiloscon];
//cantidad de items que debe crear cada hilo del cons y productor
		
		for(int i = 0; i<nhilosprod; i++){
			if((nelementsprod%nhilosprod>0)  &&  i==nhilosprod-1){
				ArrProductores[i]=((int)nelementsprod/nhilosprod) + nelementsprod%nhilosprod;			

			}else{	ArrProductores[i]=((int)nelementsprod/nhilosprod);			}	
		}
		for(int i = 0; i<nhiloscon; i++){
			if((nelementsprod%nhiloscon>0)&&i==nhiloscon-1){
				ArrConsumidor[i]=((int)nelementsprod/nhiloscon) + nelementsprod%nhiloscon;			
			}else{
			ArrConsumidor[i]=((int)nelementsprod/nhiloscon);
			}
		}

//llenar datos de la structura
		Estruct *pr = (Estruct*)malloc(sizeof(Estruct));
		pr->sb=sb;
		pr->cnt=0;
		pr->ElemProd=nelementsprod;
		pr->useOfSema=1;
		printf("%s",argv[9]);
//Arreglo de Hilos de prod y consumi
		pthread_t Pthr_Producer[nhilosprod];
		pthread_t Pthr_Consumer[nhiloscon];
		memset(Pthr_Producer,0,nhilosprod*sizeof(pthread_t));
		memset(Pthr_Consumer,0,nhiloscon*sizeof(pthread_t));

//Creates y Joins
    		for(int i=0;i<nhilosprod;i++){
			pthread_t id;
			pr->num=ArrProductores[i];
			pthread_create(&id, NULL,productor,(void*)pr);
			Pthr_Producer[i]=id;
		}
		for(int i=0;i<nhiloscon;i++){
			pthread_t id;
			pr->num=ArrConsumidor[i];
			pthread_create(&id, NULL,consumidor,(void*)pr);
			Pthr_Consumer[i]=id;
		}
		
		for (int i = 0;  i< nhilosprod; i++) {
			pthread_join(Pthr_Producer[i], NULL);
		}
		
		for (int i = 0;  i< nhiloscon; i++) {
			pthread_join(Pthr_Consumer[i], NULL);
		}

		
		sbuf_definit(sb);
		free(sb);
		free(pr);
		
	}

	if(cflag & pflag & nflag & eflag){

		//Sin semaforossss
		
		sbuf_t *sb = (sbuf_t*)malloc(sizeof(sbuf_t));
		sbuf_init(sb, tambuff);
		int ArrProductores[nhilosprod];
		int ArrConsumidor[nhiloscon];
//cantidad de items que debe crear cada hilo del cons y productor
		
		for(int i = 0; i<nhilosprod; i++){
			if((nelementsprod%nhilosprod>0)  &&  i==nhilosprod-1){
				ArrProductores[i]=((int)nelementsprod/nhilosprod) + nelementsprod%nhilosprod;			

			}else{	ArrProductores[i]=((int)nelementsprod/nhilosprod);			}	
		}
		for(int i = 0; i<nhiloscon; i++){
			if((nelementsprod%nhiloscon>0)&&i==nhiloscon-1){
				ArrConsumidor[i]=((int)nelementsprod/nhiloscon) + nelementsprod%nhiloscon;			
			}else{
			ArrConsumidor[i]=((int)nelementsprod/nhiloscon);
			}
		}

//llenar datos de la structura
		Estruct *pr = (Estruct*)malloc(sizeof(Estruct));
		pr->sb=sb;
		pr->cnt=0;
		pr->ElemProd=nelementsprod;
//Arreglo de Hilos de prod y consumi
		pthread_t Pthr_Producer[nhilosprod];
		pthread_t Pthr_Consumer[nhiloscon];
		memset(Pthr_Producer,0,nhilosprod*sizeof(pthread_t));
		memset(Pthr_Consumer,0,nhiloscon*sizeof(pthread_t));

//Creates y Joins
    		for(int i=0;i<nhilosprod;i++){
			pthread_t id;
			pr->num=ArrProductores[i];
			pthread_create(&id, NULL,productor,(void*)pr);
			Pthr_Producer[i]=id;
		}
		for(int i=0;i<nhiloscon;i++){
			pthread_t id;
			pr->num=ArrConsumidor[i];
			pthread_create(&id, NULL,consumidor,(void*)pr);
			Pthr_Consumer[i]=id;
		}
		
		for (int i = 0;  i< nhilosprod; i++) {
			pthread_join(Pthr_Producer[i], NULL);
		}
		
		for (int i = 0;  i< nhiloscon; i++) {
			pthread_join(Pthr_Consumer[i], NULL);
		}

		
		sbuf_definit(sb);
		free(sb);
		free(pr);

	}
	return 0;

}

