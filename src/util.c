#include <stdio.h>
#include <pthread.h>
#include <getopt.h>
#include <stdlib.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include "../include/util.h"

//Obtenida del libro
void sbuf_init(sbuf_t *sb, int n){
	sb->buf= calloc(n, sizeof(int));
	sb->n=n;
	sb->front=sb->rear=0;
	sem_init(&sb->mutex,0,1);
	sem_init(&sb->slots,0,n);
	sem_init(&sb->items,0,0);

}
//Obtenida del libro
void sbuf_definit(sbuf_t *sb){
	free(sb->buf);
}
//Obtenida del libro
void sbuf_insert(sbuf_t *sb, int item ){
	sem_wait(&sb->slots);
	sb->buf[(++sb->rear)%(sb->n)] = item;
	sem_post(&sb->items);
}
void sbuf_insertNoMutex(sbuf_t *sb, int item ){
	sem_wait(&sb->slots);
	sb->buf[(++sb->rear)%(sb->n)] = item;
	sem_post(&sb->items);
}
//Obtenida del libro
int sbuf_remove(sbuf_t *sb){
    int item;
    sem_wait(&sb->items);
    /* Wait for available item */
    sem_wait(&sb->mutex);
    /* Lock the buffer */
    item = sb->buf[(++sb->front)%(sb->n)]; /* Remove the item */
    sem_post(&sb->mutex);
    /* Unlock the buffer */
    sem_post(&sb->slots);
    /* Announce available slot */
    return item;
}

int sbuf_removeNoMutex(sbuf_t *sb){
    int item;
    sem_wait(&sb->items);
    /* Wait for available item */
    sem_wait(&sb->mutex);
    /* Lock the buffer */
    item = sb->buf[(++sb->front)%(sb->n)]; /* Remove the item */
    sem_post(&sb->mutex);
    /* Unlock the buffer */
    sem_post(&sb->slots);
    /* Announce available slot */
    return item;
}
//funcion obtener tiempo taller anterior
double obtener_tiempo(){
	struct timespec tsp;
	clock_gettime(CLOCK_REALTIME, &tsp);
	double secs = (double)tsp.tv_sec;
	double nsecs = (double)tsp.tv_nsec / 1000000000.0f;

	return secs + nsecs;

}
//cargo la estructura y creo los nuumeros aleatorios 
void *productor(void* args){
	Estruct *p=(Estruct*)args;
	sbuf_t *sb = (sbuf_t*)p->sb;
	for(int i = 0; i<p->num; i++){
		if(p->cnt < p->ElemProd){
			p->cnt=p->cnt+1;
			int ran = 0;
			ran = rand() % 100;
			p->tini=obtener_tiempo();
			if(p->useOfSema==1)		sbuf_insert(sb,ran);
			else sbuf_insertNoMutex(sb,ran);
			printf("El hilo: %ld produjo un item. Hay %d elementos en el buffer.\n", pthread_self(), sb->rear-sb->front );

		}else{		printf("Se han producido los: %d items\n", p->ElemProd );		
		}
		usleep(1);
	
	}
	return NULL;
}

void *consumidor(void* args){
	Estruct *p=(Estruct*)args;
	sbuf_t *sb = (sbuf_t*)p->sb;
	int n = 0;
	for(int i=0;i<p->num;i++){
		
		if(p->useOfSema==1)		n = sbuf_remove(sb);
		else n = sbuf_remove(sb);
		p->tfinal=obtener_tiempo();
		printf("Items promedio %d , Tiempo promedio de Atencion %.6f\n", sb->rear-sb->front, p->tfinal-p->tini );
		printf("El hilo: %ld consumio un item. Hay %d elementos en el buffer.\n", pthread_self(), sb->rear-sb->front);
		printf("Se ha removido el %d\n", n);
		usleep(3);
	}
	return NULL;
}
