typedef struct {
	int *buf;
	int n;
	int front;
	int rear;
	sem_t mutex;
	sem_t slots;
	sem_t items;
} sbuf_t;

typedef struct {
	int cnt;
	int ElemProd;
	int num;
	sbuf_t *sb;
	double tfinal;
	double tini;
	int useOfSema;
} Estruct;

void sbuf_init(sbuf_t *sb, int n);

void sbuf_definit(sbuf_t *sb);

void sbuf_insert(sbuf_t *sb, int item );

void sbuf_insertNoMutex(sbuf_t *sb, int item );

int sbuf_remove(sbuf_t *sb);

int sbuf_removeNoMutex(sbuf_t *sb);

double obtener_tiempo();

void *productor(void* args);

void *consumidor(void* args);